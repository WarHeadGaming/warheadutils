# WarHeadUtils

WarHeadUtils is an open source utilities plugin for the Spout Platform.

It contains the source for JDom2, CanaryMod Database API, VisualIllusionsEnt Utilities Library, and some of WarHead's own creations.

The licenses for all these products are inlcuded in the source, they are also packaged into the distribution Jar.

The files within this project are copyright of their respective authors.
