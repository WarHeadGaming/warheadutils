package net.canarymod.config;


import com.warhead.warheadutils.WarHeadUtils;
import java.io.File;
import net.visualillusionsent.utils.PropertiesFile;


/**
 * @author Jos Kuijpers
 * @author Jason (darkdiplomat)
 */
public class DatabaseConfiguration {
    private PropertiesFile cfg;

    public DatabaseConfiguration(String path) {
        File test = new File(path);

        if (!test.exists()) {
            WarHeadUtils.getLog().info("Could not find the database configuration at " + path + ", creating default.");
            DatabaseConfiguration.createDefault();
        }
        this.cfg = new PropertiesFile(path);
    }

    public DatabaseConfiguration(PropertiesFile cfg) {
        this.cfg = cfg;
    }
    
    private static DatabaseConfiguration instance;

    public static DatabaseConfiguration getInstance() {
        if (instance == null) {
            instance = new DatabaseConfiguration("db/");
        }
        return instance;
    }

    /**
     * Reloads the configuration file
     */
    public void reload() {
        cfg.reload();
    }

    /**
     * Get the configuration file
     */
    public PropertiesFile getFile() {
        return cfg;
    }

    /**
     * Creates the default configuration
     */
    public static void createDefault() {
        PropertiesFile config;

        config = new PropertiesFile("config" + File.separatorChar + "db.cfg");

        config.setString("databaseType", "xml");
        config.addComment("databaseType", "current accepted types include(without quotes): 'xml' or 'mysql'");
        config.setString("name", "minecraft");
        config.setString("host", "localhost");
        config.setString("username", "admin");
        config.setString("password", "admin");
        config.setInt("port", 3306);
        config.setInt("maxConnections", 5);
        config.addComment("maxConnections", "only used for mysql connection pooling.");

        config.save();
    }

    /**
     * Get the URL to the database.
     *
     * This is a combination of host, port and database
     * @return
     */
    public String getDatabaseUrl() {
        int port = getDatabasePort();

        return "jdbc:mysql://" + getDatabaseHost() + ((port == 0) ? "" : (":" + port)) + "/" + getDatabaseName();
    }

    /**
     * Get the database host, defaulting to localhost
     * @return
     */
    public String getDatabaseHost() {
        return cfg.getString("host", "localhost");
    }

    /**
     * Get the database port
     *
     * @return The configured port or 0
     */
    public int getDatabasePort() {
        return cfg.getInt("port", 0);
    }

    /**
     * Get the name of the database. Defaults to 'minecraft'
     * @return
     */
    public String getDatabaseName() {
        return cfg.getString("name", "minecraft");
    }

    /**
     * Get database user
     * This might be null if the datasource is not a password protected database type such as flatfile.
     * @return
     */
    public String getDatabaseUser() {
        return cfg.getString("username");
    }

    /**
     * Get database password.
     * This might be null if the datasource is not a password protected database type such as flatfile.
     * @return
     */
    public String getDatabasePassword() {
        return cfg.getString("password");
    }

    /**
     * Get the maximum number of concurrent connections to the database.
     * This might be null if the datasource is not a connection oriented database type such as flatfile.
     * @return
     */
    public int getDatabaseMaxConnections() {
        return cfg.getInt("maxConnections");
    }
    
    /**
     * Get database Type. will return either 'mysql' or 'xml'
     * @return
     */
    public String getDatabaseType() {
        return cfg.getString("databaseType");
    }
}
