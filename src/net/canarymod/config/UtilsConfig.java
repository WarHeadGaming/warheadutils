/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.canarymod.config;

import net.visualillusionsent.utils.PropertiesFile;

/**
 *
 * @author Somners
 */
public class UtilsConfig {
    
    private static PropertiesFile config = new PropertiesFile("plugins/WarHeadUtils/UtilsConfig.properties");
    private static String serverName;
    
    public UtilsConfig(){
        serverName = config.getString("server-name", "WarHead Gaming");
    }
    
    public static String getServerName(){
        return serverName;
    }
}
