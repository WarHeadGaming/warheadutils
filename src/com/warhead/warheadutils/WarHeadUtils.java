package com.warhead.warheadutils;

import net.canarymod.config.UtilsConfig;
import net.canarymod.config.DatabaseConfiguration;

import org.spout.api.plugin.Plugin;
import org.spout.api.plugin.PluginLogger;

/**
 *
 * @author somners
 */
public class WarHeadUtils extends Plugin{
    private static WarHeadUtils plugin;
    private static PluginLogger logger;
    private static DatabaseConfiguration dbConfig;
    private UtilsConfig config;
    
    /**
     * called when the plugin is loaded
     */
    @Override
    public void onLoad(){
        logger =(PluginLogger)super.getLogger();
        //logger.setTag(new ChatArguments().append(ChatStyle.PURPLE).append("[").append(ChatStyle.GOLD).append("WarHeadUtils").append(ChatStyle.PURPLE).append("]").append(ChatStyle.RESET));
        plugin = this;
        config = new UtilsConfig();
        dbConfig = new DatabaseConfiguration("plugins/WarheadUtils/database-config.properties");
    }

    /**
     * called when the plugin is enabled
     */
    @Override
    public void onEnable() {
        WarHeadUtils.getLog().info("WarHeadUtils is loaded");

    }

    /**
     * called when the plugin is disabled
     */
    @Override
    public void onDisable() {
        WarHeadUtils.logger.info("[WarHeadUtils] Disabled");
    }
    
    public static PluginLogger getLog(){
        return logger;
    }

}
